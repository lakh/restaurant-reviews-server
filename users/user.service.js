﻿const config = require('config.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('_helpers/db');
const User = db.User;

module.exports = {
    authenticate,
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function authenticate({ username, password }) {
    const user = await User.findOne({ username });
    if (user && bcrypt.compareSync(password, user.hash)) {
        const token = jwt.sign({ sub: user.id }, config.secret, { expiresIn: '7d' });
        return {
            ...user.toJSON(),
            token
        };
    }
}

async function checkUserRole(id, message) {
    const user = await User.findById(id);
    if (user.role !== 'admin') {
        throw message;
    }
    return true;
}

async function getAll(id) {
    await checkUserRole(id, 'Only admin can see list of users');
    return await User.find();
}

async function getById(id) {
    return await User.findById(id);
}

async function create(userParam) {
    // validate
    if (await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    // Role should be either user, owner or admin.
    if (userParam.role !== 'user' && userParam.role !== 'owner' && userParam.role !== 'admin') {
        throw 'Role is not acceptable. It should be user, owner or admin';
    }
    const user = new User(userParam);

    // hash password
    if (userParam.password) {
        user.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // save user
    await user.save();
}

async function update(id, userIdToUpdate, userParam) {
    if (id !== userIdToUpdate) {
        await checkUserRole(id, 'Only admin can update the other user');
    }
    const user = await User.findById(userIdToUpdate);

    // validate
    if (!user) throw 'User not found';
    if (user.username !== userParam.username && await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    // hash password if it was entered
    if (userParam.password) {
        userParam.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // copy userParam properties to user
    Object.assign(user, userParam);

    await user.save();
}

async function _delete(id, userIdToDelete) {
    if (id !== userIdToDelete) {
        await checkUserRole(id, 'Only admin can delete the other user');
    }
    await User.findByIdAndRemove(userIdToDelete);
}
