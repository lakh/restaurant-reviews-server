﻿const db = require('_helpers/db');
const Restaurant = db.Restaurant;
const Review = db.Review;
const User = db.User;

module.exports = {
    create,
    update,
    delete: _delete
};

async function checkUserRole(id, message) {
    const user = await User.findById(id);
    if (user.role !== 'admin') {
        throw message;
    }
    return true;
}

async function create(userId, reviewParam) {
    const user = await User.findById(userId);
    const restaurant = await Restaurant.findById(reviewParam.restaurant);
    if (user.role !== 'user') {
        throw 'Only users can create the review.';
    } else if (!restaurant) {
        throw 'Restaurant not found.'; 
    } else if (await Review.findOne({ user: userId, restaurant: reviewParam.restaurant })) {
        throw 'You already reviewed this restaurant';
    }

    const review = new Review({ user: userId, ...reviewParam });

    // save Review
    await review.save();
    // save reviews to restaurant.
    restaurant.reviews.push(review);
    await restaurant.save();
}

async function update(id, reviewIdToUpdate, reviewParam) {
    await checkUserRole(id, 'Only admin can update the review');

    const review = await Review.findById(reviewIdToUpdate);

    // validate
    if (!review) throw 'Review not found';

    // copy reviewParam properties to Review
    Object.assign(review, reviewParam);

    await review.save();
}

async function _delete(id, reviewIdToDelete) {
    await checkUserRole(id, 'Only admin can delete the Review');
    await Review.findByIdAndRemove(reviewIdToDelete);
}
