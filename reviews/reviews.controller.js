﻿const express = require('express');
const router = express.Router();
const reviewService = require('./review.service');

// routes
router.post('/create', create);
router.put('/:id', update);
router.delete('/:id', _delete);

module.exports = router;

function create(req, res, next) {
    reviewService.create(req.user.sub, req.body)
        .then(() => res.json({ status: 1, message: 'Review created successfullly' }))
        .catch(err => next(err));
}

function update(req, res, next) {
    reviewService.update(req.user.sub, req.params.id, req.body)
        .then(() => res.json({ status: 1, message: 'Review updated successfullly' }))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    reviewService.delete(req.user.sub, req.params.id)
        .then(() => res.json({ status: 1, message: 'Review deleted successfullly' }))
        .catch(err => next(err));
}
