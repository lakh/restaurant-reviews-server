﻿const express = require('express');
const router = express.Router();
const restaurantService = require('./restaurant.service');

// routes
router.post('/create', create);
router.get('/', getAll);
router.get('/:id', getById);
router.put('/:id', update);
router.delete('/:id', _delete);

module.exports = router;

function create(req, res, next) {
    restaurantService.create(req.user.sub, req.body)
        .then(() => res.json({ status: 1, message: 'Restaurant created successfullly' }))
        .catch(err => next(err));
}

function getAll(req, res, next) {
    restaurantService.getAll(req.user.sub)
        .then(restaurants => res.json(restaurants))
        .catch(err => next(err));
}

function getById(req, res, next) {
    restaurantService.getById(req.params.id)
        .then(restaurant => restaurant ? res.json(restaurant) : res.sendStatus(404))
        .catch(err => next(err));
}

function update(req, res, next) {
    restaurantService.update(req.user.sub, req.params.id, req.body)
        .then(() => res.json({ status: 1, message: 'Restaurant updated successfullly' }))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    restaurantService.delete(req.user.sub, req.params.id)
        .then(() => res.json({ status: 1, message: 'Restaurant deleted successfullly' }))
        .catch(err => next(err));
}
