﻿const db = require('_helpers/db');
const Restaurant = db.Restaurant;
const Review = db.Review;
const User = db.User;

module.exports = {
    create,
    getAll,
    getById,
    update,
    delete: _delete
};

async function checkUserRole(id, message) {
    const user = await User.findById(id);
    if (user.role !== 'admin') {
        throw message;
    }
    return true;
}

async function getAll(id) {
    const user = await User.findById(id);
    let query = {};
    if (user.role === 'owner') {
        query = { user };
    }
    const restaurant = await Restaurant.find(query).populate('reviews').populate({
        path: 'reviews',
        populate: ['user', 'comment']
    });
    return restaurant;
}

async function getById(id) {
    return await Restaurant.findById(id).populate('reviews').populate({
        path: 'reviews',
        populate: 'comment'
    });
}

async function create(userId, restaurantParam) {
    const user = await User.findById(userId);
    if (user.role !== 'owner') {
        throw 'Only users with owner role can create the restaurant.';
    }
    const restaurant = new Restaurant({ user, ...restaurantParam });

    // save Restaurant
    await restaurant.save();
    user.restaurants.push(restaurant);
    await user.save();
}

async function update(id, restaurantIdToUpdate, restaurantParam) {
    await checkUserRole(id, 'Only admin can update the restaurant');

    const restaurant = await Restaurant.findById(restaurantIdToUpdate);

    // validate
    if (!restaurant) throw 'Restaurant not found';

    // copy restaurantParam properties to Restaurant
    Object.assign(restaurant, restaurantParam);

    await restaurant.save();
}

async function _delete(id, restaurantIdToDelete) {
    await checkUserRole(id, 'Only admin can delete the Restaurant');
    await Restaurant.findByIdAndRemove(restaurantIdToDelete);
}
