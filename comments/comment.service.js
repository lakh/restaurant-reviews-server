﻿const db = require('_helpers/db');
const Review = db.Review;
const Restaurant = db.Restaurant;
const Comment = db.Comment;
const User = db.User;

module.exports = {
    create,
    update,
    delete: _delete
};

async function checkUserRole(id, message) {
    const user = await User.findById(id);
    if (user.role !== 'admin') {
        throw message;
    }
    return true;
}

async function create(userId, commentParam) {
    const user = await User.findById(userId);
    const review = await Review.findById(commentParam.review).populate('restaurant').populate({
        path: 'restaurant',
        populate: 'user'
    });
    const ownerId = review && review.restaurant && review.restaurant.user.id;
    if (user.role !== 'owner' || userId !== ownerId) {
        throw 'Only owner of this restaurant can create the comment.';
    } else if (!review) {
        throw 'Review not found.'; 
    } else if (await Comment.findOne({ user: userId, review: commentParam.review })) {
        throw 'You already commented this review';
    }

    const comment = new Comment({ user: userId, ...commentParam });

    // save Comment
    await comment.save();
    // save comment to review.
    review.comment = comment;
    await review.save();
}

async function update(id, commentIdToUpdate, commentParam) {
    await checkUserRole(id, 'Only admin can update the comment');

    const comment = await Comment.findById(commentIdToUpdate);

    // validate
    if (!comment) throw 'Comment not found';

    // copy commentParam properties to Comment
    Object.assign(comment, commentParam);

    await comment.save();
}

async function _delete(id, commentIdToDelete) {
    await checkUserRole(id, 'Only admin can delete the Comment');
    await Comment.findByIdAndRemove(commentIdToDelete);
}
