﻿const express = require('express');
const router = express.Router();
const commentService = require('./comment.service');

// routes
router.post('/create', create);
router.put('/:id', update);
router.delete('/:id', _delete);

module.exports = router;

function create(req, res, next) {
    commentService.create(req.user.sub, req.body)
        .then(() => res.json({ status: 1, message: 'Comment created successfullly' }))
        .catch(err => next(err));
}

function update(req, res, next) {
    commentService.update(req.user.sub, req.params.id, req.body)
        .then(() => res.json({ status: 1, message: 'Comment updated successfullly' }))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    commentService.delete(req.user.sub, req.params.id)
        .then(() => res.json({ status: 1, message: 'Comment deleted successfullly' }))
        .catch(err => next(err));
}
